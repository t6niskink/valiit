package Week1.Harjutused;

import java.util.Scanner;

public class ProgrammingBasics17 {

    public static void main(String[] args) {


        // Variant 1: switch case -----------------------------------------------------------------
        Scanner numberReaderScanner = new Scanner(System.in);  // System.in - pead ise konsooli väärtuse kirjutama.
        while (true) {      // Tsükkel (igavene)
            System.out.println("Sisesta number vahemikus 0-9: ");
            int insertedNumber = numberReaderScanner.nextInt(); // Anna mingi number ning kui on system.in, siis kasutaja peab selle sisestama
            if (insertedNumber < 0 && insertedNumber > 9) {
                break;
            }
            switch (insertedNumber) {
                case 0:
                    System.out.println("Null");
                    break;
                case 1:
                    System.out.println("Üks");
                    break;
                case 2:
                    System.out.println("Kaks");
                    break;
                case 3:
                    System.out.println("Kolm");
                    break;
                case 4:
                    System.out.println("Neli");
                    break;
                case 5:
                    System.out.println("Viis");
                    break;
                case 6:
                    System.out.println("Kuus");
                    break;
                case 7:
                    System.out.println("Seitse");
                    break;
                case 8:
                    System.out.println("Kaheksa");
                    break;
                case 9:
                    System.out.println("Üheksa");
                    break;
                    default:
                        System.out.println("VIGA! Ebakorrektne numbber!");
            }
        }




        // Variant 2: Scanneriga Lahendus -----------------------------------------------------------------------
//        String[] numberWords = {
//                "Null", "Üks", "Kaks", "Kolm",
//                "Neli", "Viis", "Kuus", "Seitse",
//                "Kaheksa", "Üheksa"
//        };
//
//        Scanner digitScanner = new Scanner(System.in); // Loome Scanner objekti. "System.in" - ütleb, et programmi kasutaja kirjutab väärtuse sisse.
//        int[] digitsToPrint = new int[10];
//        int ii = 0;
//        for (; ii < digitsToPrint.length; ii++) {
//            System.out.println("Sisesta number vahemikus 0-9: ");
//            String insertedValue = digitScanner.nextLine();
//            insertedValue = insertedValue.trim();
//
//            if (insertedValue.length() == 0) {
//                break;
//            } else {
//                digitsToPrint[ii] = Integer.parseInt(insertedValue);
//            }
//            if (ii == digitsToPrint.length - 1) {
//                System.out.println("Sorry, rohkem ei saa, käivita programm uuesti.");
//            }
//        }
//        for (int i = 0; i < ii; i++) {
//            int digitToPrint = digitsToPrint[i]; // Numbriline väärtus
//            System.out.println(numberWords[digitToPrint]);
//
//        }

        // Variant 3: Igav lahendus -------------------------------------------------------------------------
//         String[] numberWords = {
//                 "Null", "Üks", "Kaks", "Kolm",
//                 "Neli", "Viis", "Kuus", "Seitse",
//                 "Kaheksa", "Üheksa"
//         };
//         Scanner numberReaderScanner = new Scanner(System.in);
//         while (true) {
//             System.out.println("Sisesta number vahemikus 0-9: ");
//             String insertedNumber = numberReaderScanner.nextLine();
//             if (insertedNumber.length() == 0) {
//                 break;
//             } else {
//                 int insertedNumberDigit = Integer.parseInt(insertedNumber);
//                 System.out.println(numberWords[insertedNumberDigit]);
//             }
//         }


    }

}
