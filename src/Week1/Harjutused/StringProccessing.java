package Week1.Harjutused;

public class StringProccessing {
    public static void main(String[] args) {

        // Harjutused String proccessing
        System.out.println("Hello, World!");
        System.out.println("Hello, \"World\"!");
        System.out.println("Steven Hawking once said: \"Life would be tragic if it weren´t funny\".");
        System.out.println("Kui liita kokku sõned \"" +
                "See on teksti esimene pool \"" +
                "ning \"See on teksti teine pool\"" +
                ", siis tulemuseks saame \"" +
                "See on teksti esimene pool See on teksti teine pool\".");
        System.out.println("Elu on ilus.");
        System.out.println("Elu on 'ilus'.");
        System.out.println("Elu on \"ilus\".");
        System.out.println("Kõige rohkem segadust tekitab \"-märgi kasutamine sõne sees.");
        System.out.println("Eesti keele kõige ilusam lause on: \"Sõida tasa üle silla\"!");
        System.out.println("'Kolm' - kolm, 'neli' - neli, \"viis\".");

        // Ülesanne 1:
        /*1. Defineeri String-tüüpi muutuja tallinnPopulation​. ​Anna
        muutujale väärtus “450 000”.
        2. Kirjuta standardväljundisse järgmine lause: “Tallinnas elab
        450 000 inimest”​, kus rahvaarvu number pärineb muutujast
        tallinnPopulation​.
        3. Defineeri muutuja populationOfTallinn​ täisarvuna. Prindi
        standardväljundisse sama lause, mis punktis 2, kasutades
        muutujat populationOfTallinn​.
         */
        String tallinnPopulation = "450 000";
        System.out.println("Tallinnas elab " + tallinnPopulation + " inimest." );
        int populationOfTallinn = 450_000;
        System.out.println("Tallinnas elab " + populationOfTallinn + " inimest.");

        // Ülesande 1: Variant 2:
        int tallinnPopulation2 = 450_000;
        System.out.println(String.format("Tallinns elab %,d inimest.", tallinnPopulation2));


        // Ülesanne 2:
        /* ● Defineeri muutuja bookTitle​.
        ● Omista muutujale väärtus “Rehepapp”
        ● Prindi standardväljundisse tekst: “Raamatu “Rehepapp” autor
        on Andrus Kivirähk”​, kus raamatu nimi pärineb muutujast
        bookTitle​.
         */
        String bookTitle = "Rehepapp";
        System.out.println("Raamatu " + bookTitle + " autor on Andrus Kivirähk");

        // Ülesanne 3:
        /* ● Defineeri muutujad
        planet1 ​väärtusega “Merkuur”
        planet2 ​väärtusega “Venus”
        planet3 ​väärtusega “Maa”
        planet4 ​väärtusega “Marss”
        planet5 ​väärtusega “Jupiter”
        planet6 ​väärtusega “Saturn”
        planet7 ​väärtusega “Uran”
        planet8 ​väärtusega “Neptuun”
        planetCount​ väärtusega 8
        Prindi standardväljundisse lause: “Merkuur, Veenus, Maa,
        Marss, Jupiter, Saturn, Uraan ja Neptuun on Päikesesüsteemi 8
        planeeti”​, kasutades eelnevalt defineeritud muutujaid.
        Prindi standardväljundisse sama lause, aga kasuta seejuures
        abifunktsiooni String.format()​.
         */

        String planet1 = "Merkuur";
        String planet2 = "Venus";
        String planet3 = "Maa";
        String planet4 = "Marss";
        String planet5 = "Jupiter";
        String planet6 = "Saturn";
        String planet7 = "Uran";
        String planet8 = "Neptuun";
        byte planetCount = 8;
        System.out.println(planet1 + ", " + planet2 + ", " + planet3 + ", " + planet4 + ", " +
                planet5 + ", " + planet6 + ", " + planet7 + " ja " + planet8 +
                " on Päikesesüsteemi " + planetCount + " planeeti.");
        System.out.println(String.format("%s, %s, %s, %s, %s, %s, %s ja %s on Päikesesüsteemi %d planeeti.",
                planet1, planet2, planet3, planet4, planet5, planet6, planet7, planet8, planetCount));



    }

}
