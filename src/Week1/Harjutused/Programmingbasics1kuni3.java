package Week1.Harjutused;

public class Programmingbasics1kuni3 {
    public static void main(String[] args) {
        // Ülesanne 2:
        String city1 = "Berlin";
        if (city1.equals("Milano")) {
            System.out.println("Ilm on ilus.");
        } else {
            System.out.println("Ilm polegi kõige tähtsam.");
        }

        // Ülesanne 3:
        int grade = Integer.parseInt(args[0]);
        if (grade == 1) {
            System.out.println("Nõrk");
        } else if (grade == 2) {
            System.out.println("Mitterahuldav");
        } else if (grade == 3) {
            System.out.println("Rahuldav");
        } else if (grade == 4) {
            System.out.println("Hea");
        } else if (grade == 5) {
            System.out.println("Väga hea");
        }


        // Ülessanne 4:
        int hinne = 4;
        switch (hinne) {
            case 1:
                System.out.println("Nõrk");
                break;
            case 2:
                System.out.println("Mitterahuldav");
                break;
            case 3:
                System.out.println("Rahuldav");
                break;
            case 4:
                System.out.println("Hea");
                break;
            case 5:
                System.out.println("Väga hea");
                break;
            default:
                System.out.println("Ei ole hinne.");
                break;
        }

        // Ülesanne 5:
        int age = 100;
        String textToPrint = age <= 100 ? "Vana" : "Noor";
        System.out.println(textToPrint);

        // Ülesanne 6:
        textToPrint = (age < 100) ? ("Noor") : ((age == 100) ? ("Peaagu vana") : ("Vana"));
        System.out.println(textToPrint);



    }
}
