package Week1.Harjutused;

import java.util.Scanner;

public class ProgrammingBasics11kuni15 {
    public static void main(String[] args) {

//        for (int i = 0; i <= 100; i++) {
//            System.out.println(i);
//        }
//        for (int ii = 100; ii > 0; ii-=5) {
//            System.out.println(ii);
//        }


//        boolean notEnoughMoney = false;
//        int sum = 0;
//        while (notEnoughMoney) {
//            sum = sum + 50;
//            if (sum >= 5000) {
//                notEnoughMoney = false;
//            }
//        }
//        System.out.println("Potentsiaalne teenitav raha: " + sum);


//        Scanner scanner = new Scanner(System.in); // Scanner mis kuulab käsurida
//        int insertedValue = 0;
//        int checkNumber = 5;
//        do {
//            System.out.println("Sisesta palun maagiline number: \n");
//
//            insertedValue = Integer.parseInt(scanner.nextLine().trim()); // Trim võtab algusest ja lõpust kõik whitespaceid.
//        } while (checkNumber != insertedValue); // Tõene siis kui üks ei võrdu teisega.
//
//        System.out.println("Tubli, arvasid ära!");


//        // for-each tsükkel
//        int[] m = {5,1,3,7,8};
//        for (int x : m) {  // each ei kirjuta, aga sulgude sisse paned kooloni":".
//            //
//
//        }


//        // forEach tsükkel
//        String[] texts = {"Tallinn", "Tartu", "Narva"};
//        for (String temporaryTextValue : texts) { // Kasutasin koolonit ":".
//            System.out.println(temporaryTextValue + " on Eesti linn.");
//        }


        // "do" tsükkel tehakse alati läbi, hoolimata kas tulemus on true or false.
        // "do" on oma olemuselt sarnane while tsüklile.
        // do {
        // } while (boolean-avaldis)

        // TSÜKLID
        // Eesmärk teha mingit tegevust mitu korda järjest.
        // for, while, do-while, for-each
        // Selleks, et tsüklit peatada peab kasutama "break". Vastand sellele on "continue".
        // Kõige levinum tsükkel on "for".

        // for (int kasvavArv = 0; kasvavArv < 10; kasvavaArv++) {
        // }

        // while (boolean-avalid) {
        // }

//        // Harjutus 11:
//        int numberToPrint = 1;
//        while (numberToPrint <= 100) {
//            System.out.println(numberToPrint++);
//        }
//
//        // Ülesanne 12:
//        for (int i = 1; i <= 100; i++) {
//            System.out.println(i);
//        }
//
//        // Ülesanne 13:
//        // Variant 1:
//        int[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
//        for (int num2 : numbers) {
//            System.out.println(num2);
//        }
//
//        // Variant 2:
//        for (int num2 : new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}) {
//            System.out.println(num2);
//        }
//
//        // Ülesanne 14:
//        for (int i = 1; i <= 100; i++) {
//            if (i % 3 == 0)
//            System.out.println(i);
//        }

//        // Ülesanne 15:
//        String[] bands = {"Sun", "Metsatöll", "Queen", "Metallica"};
//        String commaSeparatedValues = "";
//        for (int i = 0; i < bands.length; i++){
//            commaSeparatedValues = commaSeparatedValues + bands[i];
//            if (i + 1 < bands.length) {
//                commaSeparatedValues = commaSeparatedValues + ", ";
//            }
//        }
//        System.out.println(commaSeparatedValues);


        // Ülesanne 16:
        String[] bands = {"Sun", "Metsatöll", "Queen", "Metallica"};
        String commaSeparatedValues = "";
        for (int i = bands.length; i > 0; i--){
            commaSeparatedValues = commaSeparatedValues + bands[i-1];
            if (1 < bands.length && i != 1) {
                commaSeparatedValues = commaSeparatedValues + ", ";
            }
        }
        System.out.println(commaSeparatedValues);

        }

    }
