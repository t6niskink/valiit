package Week1.Harjutused;

public class ProgrammingBasics7Kuni10 {
    public static void main(String[] args) {
        // int[] m = new int[10]; // vastus: {0,0,0,0,0,0,0,0,0,0}
        // boolean[] b = new boolean[9]; // vastus: {false,false,false,false,false,false,false,false,false,false,}

        // Mitmetasandiline massiiv
        // String[][] employees = {{"Jüri", "Ratas"}, {"Rein", "Veidemann"}, {"Kersti", "Kaljulaid"}, {"Lennart", "Meri"}};

        // Harjutused: Programming Basics
        // Ülesanne 7:
        int[] i;
        i = new int[5];
        i[0] = 1;
        i[1] = 2;
        i[2] = 3;
        i[3] = 4;
        i[4] = 5;
        System.out.println(String.format("Massiivi elemendi väärtus: %s", i[0]));
        System.out.println(String.format("Massiivi elemendi väärtus: %s", i[2]));
        System.out.println(String.format("Massiivi elemendi väärtus: %s", i[i.length - 1]));

        // Ülesanne 8:
        String[] cities = {"Tallinn", "Helsinki", "Madrid", "Paris"};

        // Ülesanne 9:
        // Variant 1: Lihtsam
        int[][] kaksTasandit = {
                {1,2,3},
                {4,5,6},
                {7,8,9,0}
        };
        // Variant 2: Raskem
        int[][] kaksTasandit2 = new int[3][];
        kaksTasandit2[0] = new int[3];
        kaksTasandit2[0][0] = 1;
        kaksTasandit2[0][1] = 2;
        kaksTasandit2[0][2] = 3;
        kaksTasandit2[1] = new int[3];
        kaksTasandit2[1][0] = 4;
        kaksTasandit2[1][1] = 5;
        kaksTasandit2[1][2] = 6;
        kaksTasandit2[2] = new int[4];
        kaksTasandit2[2][0] = 7;
        kaksTasandit2[2][1] = 8;
        kaksTasandit2[2][2] = 9;
        kaksTasandit2[2][3] = 0;
        // Seda on vaja teha nii, siis kui sa ei tea kui pikad on tsüklid.

        // Ülesanne 10:
        // Variant 1: In-line põhimõte
        String[][] linnad = {
                {"Tallin", "Tartu", "Valga", "Võru"},
                {"Stockholm", "Uppsala", "Lund", "Köping"},
                {"Helsinki", "Espoo", "Hanko", "Jämsä"}
        };

        // Variant 2: Rea haaval
        String[][] linnad2 = new String[3][];
        linnad2[1] = new String[] {"Tallinn", "Tartu", "Valga", "Võru"};
        linnad2[2] = new String[] {"Stockholm", "Uppsala", "Lund", "Köping"};
        linnad2[3] = new String[] {"Helsinki", "Espoo", "Hanko", "Jämsä"};

        // Element-haaval
        String[][] linnad3 = new String[3][];
        linnad3[0] = new String[4];
        linnad3[0][0] = "Tallinn";
        linnad3[0][1] = "Tartu";
        linnad3[0][2] = "Valga";
        linnad3[0][3] = "Võru";
        linnad3[1] = new String[4];
        linnad3[1][0] = "Stockholm";
        linnad3[1][1] = "Tartu";
        linnad3[1][2] = "Valga";
        linnad3[1][3] = "Võru";
        linnad3[2] = new String[4];
        linnad3[2][0] = "Helsinki";
        linnad3[2][1] = "Espoo";
        linnad3[2][2] = "Hanko";
        linnad3[2][3] = "Jämsä";







    }
}
