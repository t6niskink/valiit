package Week1.Harjutused;

import java.util.LinkedList;
import java.util.Queue;

public class ProgrammingBasics20 {
    public static void main(String[] args) {
        // Queue-tüüp
        // Saad võtta eest või tagant asju välja võtta. Või kui lisan siis ette.
        // Ülesanne 20:
        Queue<String> names = new LinkedList<>();
        names.add("Mary");
        names.add("Mart");
        names.add("Mark");
        names.add("Mars");
        names.add("Mare");
        names.add("Maru");

        while (!names.isEmpty()){
            System.out.println("Tõmbasin queuest sellise nime: " + names.remove());
        }
    }
}

