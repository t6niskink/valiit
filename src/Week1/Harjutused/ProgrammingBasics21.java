package Week1.Harjutused;

import com.sun.jdi.Value;
import com.sun.tools.javac.Main;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class ProgrammingBasics21 {
    public static void main(String[] args) {
        Set<String> element = new TreeSet<>();
        element.add("Tõnis");
        element.add("Mart");
        element.add("Madis");
        element.add("Kristo");
        element.add("Rico");
        element.forEach(elementToPrint -> System.out.println("Lambda-avaldis prindib: " + elementToPrint));
        // element.forEach(System.out::println);


        // Ülesanne 22:
        Map<String, String[]> citiesInCountry = new HashMap<>();
        citiesInCountry.put("Estonia", new String[]{"Tallinn", "Tartu", "Valga", "Võru"});
        citiesInCountry.put("Sweden", new String[]{"Stockholm", "Uppsala", "Lund", "Köping"});
        citiesInCountry.put("Finland", new String[]{"Helsinki", "Espoo", "Hanko", "Jämsä"});

        for (String country : citiesInCountry.keySet()) {
            System.out.println("Country: " + country);
            String[] currentCoutryCities = citiesInCountry.get(country);
            System.out.println("Cities: ");
            for (String city : currentCoutryCities){
                System.out.println("\t" + city);
            }
        }


//        citiesInCountry.forEach(print -> System.out.println("Linnad: " + print));





    }
}
