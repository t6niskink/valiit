package Week1.Harjutused;

import java.math.BigInteger;

public class ProgrammingBasics1 {
    public static void main(String[] args) {
        double d1 = 456.78;
        float f1 = 456.78F;

        String t1 = "test";
        char[] c2 = {'t', 'e', 's', 't'};

        boolean b1 = true;
        boolean b2 = (5 > 3) && (5 == 5);

        int[] i1 = {5, 91, 304, 405};

        double[] d2 = {56.7, 45.8, 91.2};
        double[] d3 = new double[3];
        d3[0] = 56.7;
        d3[1] = 45.8;
        d3[2] = 91.2;

        char c1 = 'a';

        String[] s2 = {"see on esimene väärtus", "67", "58.92"};

        // BigDecimal - komakohaga arvud, BigInteger - täisaervud.
        BigInteger bigNumber = new BigInteger("7676868683452352345324534534523453245234523452345234523452345");
        bigNumber = bigNumber.multiply(new BigInteger("2"));
        bigNumber = bigNumber.divide(new BigInteger("78945612"));

        System.out.println(bigNumber);

    }

}
