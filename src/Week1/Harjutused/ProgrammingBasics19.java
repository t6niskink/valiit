package Week1.Harjutused;

import java.util.ArrayList;
import java.util.List;

public class ProgrammingBasics19 {
    public static void main(String[] args) {
        List<String> cities = new ArrayList<>();
        cities.add("Tallinn");
        cities.add("Tartu");
        cities.add("Pärnu");
        cities.add("Tapa");
        cities.add("Viljandi");

        System.out.println("Esimene linn on: " + cities.get(0));
        System.out.println("Kolmas linn on: " + cities.get(2));
        System.out.println("Viimane linn on: " + cities.get(4));
    }
}
