package Week1.Harjutused;

public class ProgrammingBasics18 {
    public static void main(String[] args) {
        // Ülesanne 18: do-while
        double randomNumber;                 // Juhusliku numbrit saab Math.random(); käsuga.
        do {                                 // Teeb tsükli vähemalt ühe korra läbi ja siis kontrollib, kas ta jätkab.
            System.out.println("Tere");
                randomNumber = Math.random();
        }while (randomNumber < 0.5);
    }
}
