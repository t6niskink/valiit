package Week1;

import java.util.ArrayList;
import java.util.List;

public class Day04Kollektsioonid {
    public static void main(String[] args) {
//        // Kollektsioonide kolm tüüpi:
//        // * List, ArryList
//        // * Set, HashSet, TreeSet. Kui tahad lisada unikaalseid asju (ei tohi korduda).
//        // * Map, HashMap           Nime ja väärtuse paarid. Tahad teha päringuid nt: töötajad ja nende isikukoodid.
//
//        // Need ei ole klassid, need on interfaceid (Liides kuidas välismaailm saab sellega suhelda.
//        // Nad defineerivad ära kuidas üks või teine peab välja nägema.
//
//        // Primitiivseid klasse peame kasutama. Integer mitte int
//
//
//        // LIST
//        List<Integer> numbersList = new ArrayList<>();
//
//        numbersList.add(4);
//        numbersList.add(7);
//
//        for (Integer x : numbersList) {
//            System.out.println(x);
//        }
//
        // SET
        // Set hoiab endas unikaalseid väärtuseid. Kui lisad kaks korda 4, siis ta hoiab ikkagi ainult ühte 4.
//        Set<Integer> uniqueNumbersSet = new HashSet<>();
//
//        uniqueNumbersSet.add(4);
//        uniqueNumbersSet.add(7);
//
//        for (Integer x : uniqueNumbersSet){
//            System.out.println(x);
//        }
//
//        // MAP
//        // Map koosneb kahest objektist!! Nimi ja väärtus.
//        Map<String, Integer> ages = new HashMap<>();
//
//        ages.put(„Mati“, 25);
//        ages.put(„Kati“, 30);
//        ages.put(„Tõnu“, 35);
//        for (String nameKey: ages) {
//            System.out.println(nameKey + „ on “ + ages.get(nameKey) + „ aastane.“);
//        }
//        // .get, anna mulle väärtus kohal().
//        // .values




    }
}
