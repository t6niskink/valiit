package Week1;

import java.lang.reflect.Array;
import java.util.*;

public class Day04KollektsioonideNäited {
    public static void main(String[] args) {
        // List
        List<String> names = new ArrayList<>();

        names.add("Tõnis");
        names.add("Marek");
        names.add("Karl");
        System.out.println(names);

        int insertPosition = names.indexOf("Marek");  // .indexOf ütleb, Kus "Marek" asub.
        System.out.println(insertPosition);
        names.add(2, "Madis");       // Lisab nime vahele. number -> koma -> ja tuleb ette index
        System.out.println(names);

        names.contains("Linda");
        System.out.println(names.contains("Linda"));  // .contains - Kas selline nimi on listis olemas?

        // Asenda element indeksi kohal x...            // Nime asendamine
        int tõnisPosition = names.indexOf("Tõnis");     // Kui nime ei ole listis siis see annaks teate -1
        if (tõnisPosition >= 0) {                           // kui on listis, siis asendab ära.
            names.set(tõnisPosition, "Laura");
        }
        System.out.println(names);


        for (String name : names) {                     // for-eachis name ei pea eelnevalt kuskil defineerima.
            System.out.println("Nimi: " + name);
        }
        System.out.println(names.get(3));  // Kindla indeksiga elemendi pärimine.


        // -------------------
        // LIST
        // -------------------
        // Mitmetasandiline List (Kahetasandiline)
        List<List<String>> complexList = new ArrayList<>();

        List<String> humans = Arrays.asList("Mark", "Mary", "Thomas", "Linda");
        complexList.add(humans);

        List<String> cars = Arrays.asList("BMW", "Audi", "Mercedes Benz", "Ford");
        complexList.add(cars);

        complexList.add(Arrays.asList("Airbus", "Boeing"));         // Kui tahan lisada listi

        System.out.println(complexList);
        // Kui tahan teada listi väärtust
        System.out.println(complexList.get(1).get(1));


        // -------------------
        // Set
        // -------------------
        // Unikaalsete elementide kogum
        Set<String> uniqueNames = new HashSet<>();

        uniqueNames.add("Mary");
        uniqueNames.add("Thomas");
        uniqueNames.add("Mark");
        uniqueNames.add("Mary");

        System.out.println(uniqueNames);
        Object[] uniqueNamesArr = uniqueNames.toArray();
        System.out.println(uniqueNames.toArray()[0]);

        for (String uniqueName : uniqueNames) {
            System.out.println("Unikaalne nimi: " + uniqueName);
        }

        // Saad võtta infot maha.
        uniqueNames.remove("Mark");
        System.out.println(uniqueNames);


        // -------------------
        // TREE SET   .add
        // -------------------
        Set<String> shortedNames = new TreeSet<>();
        shortedNames.add("Mary");
        shortedNames.add("Thomas");
        shortedNames.add("Mark");
        System.out.println(shortedNames);

        // -------------------
        // Map        .put
        // -------------------
        // Näiteks inimene ja tema tel. nr.
        Map<String, String> phoneNumbers = new HashMap<>();

        phoneNumbers.put("Mati", "+372 5622 3366");
        phoneNumbers.put("Kati", "+372 5722 3366");
        phoneNumbers.put("Nati", "+372 5822 3366");
        phoneNumbers.put("Tati", "+372 5922 3366");

        for (String key : phoneNumbers.keySet()){
            System.out.println("Sõber: " + key + ", telefon: " + phoneNumbers.get(key));
        }

        phoneNumbers.put("Mati", "+372 8899 7788");
        System.out.println(phoneNumbers);


        // TreeX - sorteerimise üle saad ise otsustada
        // HashX - Java


        Map<String, Map<String, String>> contacts = new TreeMap<>();

        Map<String, String> mariContactDetails = new TreeMap<>();
        mariContactDetails.put("Phone", "+372 5555 8888");
        mariContactDetails.put("Email", "Mari@kari.ee");
        mariContactDetails.put("Aadress", "Uus 8, Tallinn");
        contacts.put("Mari", mariContactDetails);

        Map<String, String > martContactDetails = new TreeMap<>();
        martContactDetails.put("Phone", "+372 5555 8888");
        martContactDetails.put("Email", "Mari@kari.ee");
        martContactDetails.put("Aadress", "Uus 8, Tallinn");
        contacts.put("Mart", martContactDetails);

        System.out.println(contacts);
        System.out.println("Mardi telefoninumber: " + contacts.get("Mart").get("Phone"));





    }
}
