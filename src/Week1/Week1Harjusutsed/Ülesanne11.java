package Week1.Week1Harjusutsed;

import java.util.*;

public class Ülesanne11 {
    public static void main(String[] args) {
        Queue<List<Object>> countryMap = new LinkedList<>();
        countryMap.add(Arrays.asList("Estonia", "Tallinn", "Jüri Ratas", Arrays.asList("Estonian", "Russian", "Finnish", "English")));
        countryMap.add(Arrays.asList("Finland", "Helsinki", "Antti Rinne", Arrays.asList("Finnish", "Swedish", "Skolt Sami", "Meänkieli")));
        countryMap.add(Arrays.asList("Sweden", "Stockholm", "Stefan Löfven", Arrays.asList("Finnish", "Swedish", "Southern Sami", "Meänkieli")));
        countryMap.add(Arrays.asList("Denmark", "Copenhagen", "Mette Frederiksen", Arrays.asList("German", "Danish", "Greenlandic", "Danish Sign Language")));
        countryMap.add(Arrays.asList("France", "Paris", "Emmanuel Macron", Arrays.asList("French", "Fang language", "Barama", "Baka language")));
        countryMap.add(Arrays.asList("Germany", "Berlin", "Angela Merkel", Arrays.asList("German", "Danish", "Luxembourgish", "Upper Sorbian")));
        countryMap.add(Arrays.asList("Belgium", "Brussels", "Charles Michel", Arrays.asList("French", "German", "Dutch", "Luxembourgish")));
        countryMap.add(Arrays.asList("Italy", "Rome", "Giuseppe Conte", Arrays.asList("French", "German", "Italian", "Slovene")));
        countryMap.add(Arrays.asList("Spain", "Madrid", "Pedro Sánchez", Arrays.asList("Spanish", "Portuguese", "Catalan", "Basque")));

        while (!countryMap.isEmpty()){
            List<Object> country = countryMap.remove();
            System.out.println(country.get(1) + ": ");
            for (String language : (List<String>)country.get(3)){
                System.out.println(language + " ");
            }
            System.out.println();
            System.out.println();
        }
    }
}
