package Week1.Week1Harjusutsed;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Ülesanne10 {
    public static void main(String[] args) {
        Map<String, List<Object>> countryMap = new HashMap<>(); // TreeMapi puhul saaks tähestiku järjekorras.
        countryMap.put("Estonia", Arrays.asList("Tallinn", "Jüri Ratas", Arrays.asList("Estonian", "Russian", "Finnish", "English")));
                countryMap.put("Latvia", Arrays.asList("Riga", "Arturs Krišjānis Kariņš", Arrays.asList("Russian", "Latvian", "Livonian", "Latgalian")));
                countryMap.put("Finland", Arrays.asList("Helsinki", "Antti Rinne", Arrays.asList("Finnish", "Swedish", "Skolt Sami", "Meänkieli")));
                countryMap.put("Sweden", Arrays.asList("Stockholm", "Stefan Löfven", Arrays.asList("Finnish", "Swedish", "Southern Sami", "Meänkieli")));
                countryMap.put("Denmark", Arrays.asList("Copenhagen", "Mette Frederiksen", Arrays.asList("German", "Danish", "Greenlandic", "Danish Sign Language")));
                countryMap.put("France", Arrays.asList("Paris", "Emmanuel Macron", Arrays.asList("French", "Fang language", "Barama", "Baka language")));
                countryMap.put("Germany", Arrays.asList("Berlin", "Angela Merkel", Arrays.asList("German", "Danish", "Luxembourgish", "Upper Sorbian")));
                countryMap.put("Belgium", Arrays.asList("Brussels", "Charles Michel", Arrays.asList("French", "German", "Dutch", "Luxembourgish")));
                countryMap.put("Italy", Arrays.asList("Rome", "Giuseppe Conte", Arrays.asList("French", "German", "Italian", "Slovene")));
                countryMap.put("Spain", Arrays.asList("Madrid", "Pedro Sánchez", Arrays.asList("Spanish", "Portuguese", "Catalan", "Basque")));
                for (String country : countryMap.keySet()) {
                    System.out.println(country + ":");
                }
    }
}
