package Week1.Week1Harjusutsed;

public class Ülesanne4 {

    public static void main(String[] args) {

        // Variant 1:
        int number = 1234567;
        String numberText = String.valueOf(number); // Saan numbrist teksti // "1234567"
        char[] numberChars = numberText.toCharArray();// "1234567" -> {'1', '2', '3', ...}
        String reversedNumberText = "";
        for (int i = 0; i < numberChars.length; i++){
            reversedNumberText = numberChars[i] + reversedNumberText;
        }
        // Sama for näide for-eachi kohta.
        // for(char c : numberChars) {
        //    reversedNumberText = numberChars[i] + reversedNumberText:

        // Siin tee numbri stringist täisarv
        int reversedNumber = Integer.parseInt(reversedNumberText);
        // Siin prindi see välja
        System.out.println(reversedNumber);


        // ------------------------------
        System.out.println();
        // Variant 2
        int number2 = 1234567;
        String numberText2 = String.valueOf(number2);
        StringBuilder reverseNumberBuilder = new StringBuilder(numberText2);
        String reverseNumberText2 = reverseNumberBuilder.reverse().toString();
        int reversedNumber2 = Integer.parseInt(reverseNumberText2);
        System.out.println(reversedNumber2);


        // Kõige lühem viis
        System.out.println();
        System.out.println(new StringBuilder("1234567").reverse().toString());
    }
}
