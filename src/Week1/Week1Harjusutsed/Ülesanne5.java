package Week1.Week1Harjusutsed;

import java.util.Scanner;

public class Ülesanne5 {
    public static void main(String[] args) {

        // Minu näide
        String name = String.valueOf(args[0]);
        int grade = Integer.parseInt(args[1]);
        if (grade < 51) {
            System.out.println("FAIL");
        } else if (grade < 61) {
            System.out.println(name + ": PASS - Hinne: 1 Punktid: " + grade);
        } else if (grade < 71) {
            System.out.println(name + ": PASS - Hinne: 2 Punktid: " + grade);
        } else if (grade < 81) {
            System.out.println(name + ": PASS - Hinne: 3 Punktid: " + grade);
        } else if (grade < 91) {
            System.out.println(name + ": PASS - Hinne: 4 Punktid: " + grade);
        } else if (grade <= 100) {
            System.out.println(name + ": PASS - Hinne: 5 Punktid: " + grade);
        } else {
            System.out.println("Oled liiga tark");
        }

        // Mareku variant
        if (args.length >= 2) {  // Kas on üldse parameetreid, mida küsida?
            String studentName = args[0];  // args - main() meetodi sisendparameeter
            int studentScore = Integer.parseInt(args[1]);
            String resultingText = studentName + ": ";
            // Siia tuleb resulting text teha ...
            if (studentScore > 90) {
                resultingText = resultingText + "PASS - 5, " + studentScore;
            } else if (studentScore > 80) {
                resultingText = resultingText + "PASS - 4, " + studentScore;
            } else if (studentScore > 70) {
                resultingText = resultingText + "PASS - 3, " + studentScore;
            } else if (studentScore > 60) {
                resultingText = resultingText + "PASS - 2, " + studentScore;
            } else if (studentScore > 50) {
                resultingText = resultingText + "PASS - 1, " + studentScore;
            } else {
                resultingText = "FAIL";
            }
            System.out.println(resultingText);
        }
    }
}
