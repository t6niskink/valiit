package Week1.Week1Harjusutsed;

public class Ülesanne7 {
    public static void main(String[] args) {
        String[][] countryCitiesPrime = {
                {"Estonia", "Tallinn", "Jüri Ratas"},
                {"Latvia", "Riga", "Arturs Krišjānis Kariņš"},
                {"Finland","Helsinki","Antti Rinne"},
                {"Sweden", "Stockholm", "Stefan Löfven"},
                {"Denmark", "Copenhagen", "Mette Frederiksen"},
                {"France", "Paris", "Emmanuel Macron"},
                {"Germany", "Berlin", "Angela Merkel"},
                {"Belgium", "Brussels", "Charles Michel"},
                {"Italy", "Rome", "Giuseppe Conte"},
                {"Spain", "Madrid", "Pedro Sánchez"},
        };
        // For-each peab alati olema esimese poole ees (String[]) või (Int[]) ...
        for (String[] country : countryCitiesPrime){
            System.out.println("Country: " + country[0] +
                    " Capital: " + country[1] +
                    " Prime Minister: " + country[2]);
        }
    }
}
