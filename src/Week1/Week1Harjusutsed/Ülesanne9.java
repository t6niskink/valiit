package Week1.Week1Harjusutsed;

import java.util.Arrays;
import java.util.List;

public class Ülesanne9 {
    public static void main(String[] args) {
        List<List<Object>> countries = Arrays.asList(
                Arrays.asList("Estonia", "Tallinn", "Jüri Ratas", Arrays.asList("Estonian", "Russian", "Finnish", "English")),
                Arrays.asList("Latvia", "Riga", "Arturs Krišjānis Kariņš", Arrays.asList("Russian", "Latvian", "Livonian", "Latgalian")),
                Arrays.asList("Finland", "Helsinki", "Antti Rinne", Arrays.asList("Finnish", "Swedish", "Skolt Sami", "Meänkieli")),
                Arrays.asList("Sweden", "Stockholm", "Stefan Löfven", Arrays.asList("Finnish", "Swedish", "Southern Sami", "Meänkieli")),
                Arrays.asList("Denmark", "Copenhagen", "Mette Frederiksen", Arrays.asList("German", "Danish", "Greenlandic", "Danish Sign Language")),
                Arrays.asList("France", "Paris", "Emmanuel Macron", Arrays.asList("French", "Fang language", "Barama", "Baka language")),
                Arrays.asList("Germany", "Berlin", "Angela Merkel", Arrays.asList("German", "Danish", "Luxembourgish", "Upper Sorbian")),
                Arrays.asList("Belgium", "Brussels", "Charles Michel", Arrays.asList("French", "German", "Dutch", "Luxembourgish")),
                Arrays.asList("Italy", "Rome", "Giuseppe Conte", Arrays.asList("French", "German", "Italian", "Slovene")),
                Arrays.asList("Spain", "Madrid", "Pedro Sánchez", Arrays.asList("Spanish", "Portuguese", "Catalan", "Basque"))
                );
        for (List<Object> country : countries) {
            System.out.println(country.get(0) + ":");
            for (String language : (List<String>)country.get(3)){
                System.out.println(language + " ");
            }
            System.out.println();
        }
    }
}
