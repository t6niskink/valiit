package Week1.Week1Harjusutsed;

public class Ülesanne1 {
    public static void main(String[] args) {
        // Ülesanne 1:
        // row 0 -> 6 col (veerud 0...5)
        // row 1 -> 5 col (veerud 0...4)
        // row 2 -> 4 col (veerud 0...3)
        int columnCount = 6;
        for (; columnCount > 0; columnCount--) {
            for (int column = 0; column < columnCount; column++) {
                System.out.print("#");
            }
            System.out.println("");
        }
    }
}
