package Week1.Week1Harjusutsed;

public class Ülesanne6 {
    public static void main(String[] args) {
        double[][] triangelSidesList = {
                {1.5, 7, 7},
                {8.5, 9.4},
                {3.4, 4.6},
                {6.5, 985.5},
                {65.4, 763.5},
        };
        for (double[] triangelSides : triangelSidesList) {
            double c = Math.sqrt(Math.pow(triangelSides[0], 2) + Math.pow(triangelSides[1], 2));
            System.out.println("Kolmnurga hüpotenuusi pikkus: " + c);
        }
    }
}