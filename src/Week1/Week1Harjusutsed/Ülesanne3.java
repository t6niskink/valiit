package Week1.Week1Harjusutsed;

public class Ülesanne3 {
    public static void main(String[] args) {
        int sideLength = 6;
        for (int row = 1; row < sideLength; row++) {
            for (int column = 0; column < sideLength; column++) {
                if (column < sideLength - row){
                    System.out.print(" ");
                }else {
                    System.out.print("@");
                }
            }
            System.out.println("");
        }
    }
}
