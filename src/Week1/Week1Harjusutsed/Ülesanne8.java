package Week1.Week1Harjusutsed;

public class Ülesanne8 {
    public static void main(String[] args) {
        // String on objekt
        // Stringide massiiv on ka objekt
        // String[] arr1 = {"aa", "nn"};
        // Object[] arr2 = {"aa", "nn"};
        // Object[] str1 = "Tere";
        Object[][] countryCitiesPrime = {
                {"Estonia", "Tallinn", "Jüri Ratas", new String[]{"Estonian", "Russian", "Finnish", "English"}},
                {"Latvia", "Riga", "Arturs Krišjānis Kariņš", new String[]{"Russian", "Latvian", "Livonian", "Latgalian"}},
                {"Finland", "Helsinki", "Antti Rinne", new String[]{"Finnish", "Swedish", "Skolt Sami", "Meänkieli"}},
                {"Sweden", "Stockholm", "Stefan Löfven", new String[]{"Finnish", "Swedish", "Southern Sami", "Meänkieli"}},
                {"Denmark", "Copenhagen", "Mette Frederiksen", new String[]{"German", "Danish", "Greenlandic", "Danish Sign Language"}},
                {"France", "Paris", "Emmanuel Macron", new String[]{"French", "Fang language", "Barama", "Baka language"}},
                {"Germany", "Berlin", "Angela Merkel", new String[]{"German", "Danish", "Luxembourgish",  "Upper Sorbian"}},
                {"Belgium", "Brussels", "Charles Michel", new String[]{"French", "German", "Dutch", "Luxembourgish"}},
                {"Italy", "Rome", "Giuseppe Conte", new String[]{"French", "German", "Italian", "Slovene"}},
                {"Spain", "Madrid", "Pedro Sánchez", new String[]{"Spanish", "Portuguese", "Catalan", "Basque"}},
        };
        for (Object[] country : countryCitiesPrime){
            System.out.println(country[0] + ":");
            for (String language : (String[])country[3]) {
                System.out.println(language + " ");
            }
            System.out.println();
            System.out.println();
        }

    }
}
