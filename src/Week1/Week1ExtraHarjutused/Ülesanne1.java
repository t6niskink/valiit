package Week1.Week1ExtraHarjutused;

public class Ülesanne1 {
    public static void main(String[] args) {
        int rowCount = 8;
        int colCount = 19;

        for (int row = 0; row < rowCount; row++){
            for (int col = 0; col < colCount; col++){
                if ((col + row) % 2 == 0){
                    System.out.print("#");
                }else {
                    System.out.print("+");
                }
            }
            System.out.println();
        }
    }
}
