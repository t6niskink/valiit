package Week1;

import java.util.Scanner;

public class Day03Teine {

    public static final double VAT_RATE = 0.3; // Saab muuta ainult koodi sees. Seda rohkem ei saa muuta.
    // "final" et muutujat ei saa muuta.

    public static void main(String[] args) {



        // StringBuffer ja StringBuilder
        // Vajalik mälu efektiivse kasutamise jaoks. Mõistlik suurte stringide kokkupanemisel.
        StringBuilder myStringBuilder = new StringBuilder();
        myStringBuilder.append("Tõnis, "); // append meetod - Lisab uue teksti Loodava teksti lõppu.
        myStringBuilder.append("Karl, ");
        myStringBuilder.append("Liis, ");
        myStringBuilder.append("Mart, ");
        String allTheNames = myStringBuilder.toString();
        System.out.println(allTheNames);




//        Scanner poemInput = new Scanner(System.in);
//        System.out.println("Kirjuta üks ilus luuletus:\n");
//        String poem = poemInput.nextLine(); // Süsteem ei prindi kohe "Luuletus oli selline:" ennem kui midagi ei ole süsteemi sisestatud
//        System.out.println("Luuletus oli selline: \n");
//        System.out.println(poem);





//        String myText = "Tallinn, Tartu, Valga, Rapla";
//        Scanner scanner = new Scanner(myText);
//        scanner.useDelimiter(", "); // Lõpetad ", " koha pealt ära.
//
//        while(scanner.hasNext()) { // Ketrab seda koodijuppi nii kaua, kuni sealt on veel midagi võtta
//            System.out.println(scanner.next()); // Antud näitest 4 korda, kuna on neli linna.
//        }
    }
}
