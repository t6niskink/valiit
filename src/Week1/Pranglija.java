package Week1;

import java.util.*;

public class Pranglija {

    private static final int GUESS_COUNT = 3;
    private static Scanner inputReader = new Scanner(System.in);
    private static List<String[]> log = new ArrayList<>();

    public static void main(String[] args) {




        while (true) {
            System.out.println("Mäng algab...");
            System.out.println("Sisesta oma nimi: ");
            String playerName = inputReader.nextLine();

            System.out.println("Sisesta genereeritavate arvude miinimumväärtus, mis on suurem või võrdne 0: ...");
            int minValue = getValueFromUser(0,100_000);
            System.out.println("Sisesta genereeritavate arvude maksimumväärtus: ...");
            int maxValue = getValueFromUser(minValue + 1, 100_000);
            int delta = maxValue - minValue;


            System.out.println();
            long startTime = System.currentTimeMillis();
            int i = 0;
            for (; i < 10; i++) {
                int number1 = generateRandomNumber(minValue, maxValue);
                int number2 = generateRandomNumber(minValue, maxValue);
                int sum = number1 + number2;

            System.out.println(String.format("%d + %d = ", number1, number2));
                System.out.println(number1 + " + " + number2 + " = ");
                int guessedSum = inputReader.nextInt();
                if (guessedSum != sum) {
                    break;
                }

            }

            if (i != 10) {
                System.out.println("Mäng läbi, sa kaotasid");
            } else {
                long endTime = System.currentTimeMillis();
                long elapsedSeconds = (endTime - startTime) / 1000;
                System.out.println("Mäng läbi, sul kulus " + elapsedSeconds + " sekundit!");

                // Salvestame tulemuse logisse...
                String[] result = {playerName, String.valueOf(elapsedSeconds), minValue + " ..." + maxValue};
                log.add((result));

                // Kuvame tulemused...

                System.out.println("---------------------------");
                System.out.println("Tulemused");
                System.out.println("---------------------------");

                // Sorteerime tulemused aja järgi kasvavalt.

                for (String[] resultItem : log) {
                    System.out.println("Nimi: " + resultItem[0]);
                }

            }

            System.out.println("Mäng on lõppenud, mida soovid edasi teha?");
            System.out.println("0 - Välju mängust");
            System.out.println("1 - Alusta uut mängu");
            int playerSelection;


            while (true) {
                playerSelection = inputReader.nextInt();
                if (playerSelection == 1 || playerSelection == 0) {
                    break;
                }
                System.out.println("Vigane valik, vali kas 0 või 1.");
            }

            if (playerSelection == 0) {
                break;
            }

            // Hack: muidu jääb viimane \n
            inputReader.nextLine();
        }
    }
    private static int generateRandomNumber (int minValue, int maxvalue) {
        int delta = maxvalue - minValue;
        return (int)(Math.random() * (delta + 1)) + minValue;
    }

    private static int getValueFromUser(int minValue, int maxValue) {
        while (true) {
            int userValue = 0;
            minValue = inputReader.nextInt();
            if (userValue >= minValue && userValue <= maxValue) {
                return userValue;
            } else {
                System.out.println(String.format("Ebakorrektne väärtus, number peab olema vahemikus %d ja %d", minValue, maxValue));
            }
        }
    }
}
