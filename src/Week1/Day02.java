package Week1;

import javax.sound.midi.Soundbank;

public class Day02 {
    public static void main(String[] args) {


        byte b1 = 1; //Suurus: 1 bait: väike number
        char c1 = 'B'; //Suurus: 2 bait: Üks tähemärk
        short s1 = 4578; //Suurus: 2 bait: Keskmise suurusega number
        int i1 = 56_000_000; //Suurus: 4baiti: Suured numbrid
        long l1 = 10_000_000_000_000_000L; // Suurus: 8 baiti: Väga suured numbrid
        boolean isItSpring = false; // Suurus: 1bait. Tõeväärtus- tõene väär
        float f1 = 45.678F; // Suurus: 4baiti. Komakohaga arv(võrdlemisi suur. kuid ebatäpne
        double d1 = 567.98765; // Suurus: 8 baiti. Komakohaga arv (Veelgi suurem, aga ikkagi ebatäpne)



        System.out.println((char)b1); // casting:byte -> char
        System.out.println(c1);


        // Operaatiorid
        // ==
        int i4 = 5;
        int i5 = 2;
        int i6 = 45;
        boolean test1 = i4 == i5;
        System.out.println(test1); // False


        // ++
        int i7 = 50;
        System.out.println(i7);
        System.out.println(++i7);


        // +=
        int i9 = 11;
        i9 = i9 + 5; // Variant 1
        i9 += 5; // Variant 2
        // Variant 2 on kiirem ja nagu shortcut


        // Loogiline või ||
        // Üks või teine või mõlemad;
        boolean b10 = false;
        boolean b11 = true;
        System.out.println(b10 || b11);


        // Loogiline and &&
        boolean b12 = false;
        boolean b13 = true;
        System.out.println(b12 && b13);


        // Mooduliga jagamine %
        int i14 = 11;
        int i15 = 10;
        System.out.println(i14 % 10); // Jääk on 1
        System.out.println(i15 % 10); // Jääk on 0


        // Wrapper klassid
        Integer i20 = 20;
        Integer i21 = new Integer(20);
        // Need kaks on tegelikult võrdsed.


        // Harjutus
        // Loo kolm arvulist muutujat a = 1, b = 1, c = 3
        int a1 = 1;
        int b14 = 1;
        int c6 = 3;

        // Prindi välja a == b ja a == c
        System.out.println(a1 == b14);
        System.out.println(a1 == c6);

        // Lisa koodi rida a = c
        a1 = c6;

        // Prindi välja a == b ja a == c, mis muutus ja miks?
        System.out.println(a1 == b14);
        System.out.println(a1 == c6);

        // Loo muutujad x1 = 10 ja x2 = 20, vali sobiv andmetüüp
        int x1 = 10;
        int x2 = 20;

        // Tekita muutuja y1 = ++x1, trüki välja nii x1 kui y1
        int y1 = ++x1;
        System.out.println("x1: " + x1);
        System.out.println("y1: " + y1);

        // Tekita muutuja y2 = x2++, trüki välja nii x2 ja y2
        int y2 = x2++;
        System.out.println(x2);
        System.out.println(y2);

        // Harjutus2 - Moodul
        int a = 18;
        int b = 19;
        int c = 20;
        int d = 21;
        System.out.println(a % 3); // 0
        System.out.println(b % 3); // 1
        System.out.println(c % 3); // 2
        System.out.println(d % 3); // 0


        // Stringtöötlus
        System.out.println("Isa ütles: \"Tule siia!\"");
        System.out.println("Minu faili asukoht: C:\\test\\test.txt");
        System.out.println("See on esimene rida. \nSee on teine rida");

        String myText1 = "This is a text.";
        String myText2 = "This is another text.";
        myText1 = myText1 + myText2;
        System.out.println(myText1);

        String myText3 = "T3";
        String myText4 = "T3";
        System.out.println(myText3 == myText4); // Ebakorrektne viis stringide võrdlemiseks!!!
        System.out.println(myText3.equals(myText4)); // Korrektne viis stringide võrdlemiseks.
        System.out.println(myText3.equalsIgnoreCase("t3")); // Täht ei peab olema suur
        String myText5 = new String("Some text..."); // nii võib ka, kuid see on liiga pikk ning nii üldjuhul ei tehta.






    }



}
