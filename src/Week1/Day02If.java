package Week1;

public class Day02If {
    public static void main(String[] args) {

        int temperature = Integer.parseInt(args[0]);

        if (temperature >= 30) {
            System.out.println("Ilm on ilus!");
        } else if(temperature >= 20) {
            System.out.println("Ilm ei ole ilus!");
        } else if (15 < temperature && temperature < 20) {
            System.out.println("Tegemist on Jaanipäevaga");
        } else {
            System.out.println("Väljas on külm");
        }

        //Variant 2: Switch
        // 1 - roheline tuli
        // 2 - kollane tuli
        // 3 - punane tuli
        int inputValue = 1;
        switch(inputValue) {
            case 1:
                // Tee midagi
                System.out.println("Fooris põleb roheline tuli");
                break;
            case 2:
                // Tee midagi muud
                System.out.println("Fooris põleb kollane tuli");
                break;
            case 3:
                // Tee midagi hoopis muud
                System.out.println("Fooris põleb punane tuli");
                break;
            default:
                // Siia satud siis kui eelnevad cased ei olnud tõesed
                System.out.println("Valgusfoor on rikkis");
        }

        // Variant 3: (inline-if) Selle mõte on ainult siis kui on üks või teine. Ei ole mõeldud üle kahe statementi tegemiseks.
        // String muutuja = Kas tingimus on tõene? "Jah" : "Ei";
        String weatherText = temperature >= 30 ? "Ilm on ilus" : "Ilm ei ole ilus.";
        System.out.println(weatherText);




    }
}
