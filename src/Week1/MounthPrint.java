package Week1;

public class MounthPrint {
    public static void main(String[] args) {


        int num = Integer.parseInt(args[0]);
        switch (num) {
            case 1:
                System.out.println("Jaanuar");
                break;
            case 2:
                System.out.println("Veebruar");
                break;
            case 3:
                System.out.println("Märts");
                break;
            case 4:
                System.out.println("Aprill");
                break;
            case 5:
                System.out.println("Mai");
                break;
            case 6:
                System.out.println("Juuni");
                break;
            case 7:
                System.out.println("Juuli");
                break;
            case 8:
                System.out.println("August");
                break;
            case 9:
                System.out.println("September");
                break;
            case 10:
                System.out.println("Oktoober");
                break;
            case 11:
                System.out.println("November");
                break;
            case 12:
                System.out.println("Detsember");
                break;
            default:
                System.out.println("Number on kuupäeva arvestusest väljas");
                break;
        }

    }
}
