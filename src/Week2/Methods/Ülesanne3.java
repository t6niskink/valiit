package Week2.Methods;

public class Ülesanne3 {
    public static void main(String[] args) {
        System.out.println(addVat(100.0));
    }
    static double addVat(double initialPrice) {
        return initialPrice * 1.2;
    }
}
