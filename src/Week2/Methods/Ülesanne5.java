package Week2.Methods;

public class Ülesanne5 {
    public static void main(String[] args) {
        String gender = deriveGender("495");
        System.out.println("Sugu: " + gender);

    }
    static String deriveGender(String personalCode){
        char firstDigitsChar = personalCode.charAt(0);
        String firstDigitsString = String.valueOf(firstDigitsChar);
        int firstDigitValue = Integer.parseInt(firstDigitsString);

        // Inline-if
        return firstDigitValue % 2 == 1 ? "M" : "F";

//        // Võib ka pikemalt if-ga
//        if (firstDigitValue % 2 == 1) {
//            return "M";
//        }else {
//            return "F";
//        }

    }
}
