package Week2;

public class Day01OO {
    public static void main(String[] args) {
        Human rein = new Human();
        rein.name = "Rein";
        rein.age = 45;
        rein.weight = 87;

        Human mari = new Human();
        mari.name = "Mari";
        mari.age = 28;
        mari.weight = 55;

        Human rein2 = new Human("Rein", 45, 87);
        Human mari2 = new Human("Mari", 28, 55);

        Human tõnis = new Human("Tõnis");

        System.out.println("Kas mari on noor? " + mari.isYoung());
        System.out.println("Kas mari on noor? " + Human.isThisHumanYoung(mari));

        Person personMari = new Person("48206234563");
        System.out.println("Mari info:");
        System.out.println("Sugu: " + personMari.getGender());
        System.out.println("Sünnikuu: " + personMari.getBirthMonth());
        System.out.println("Sünnikuupäev: " + personMari.getBirthDayOfMonth());
        System.out.println("Millal sündinud (aasta)? " + personMari.getBirthYear());
    }
}
