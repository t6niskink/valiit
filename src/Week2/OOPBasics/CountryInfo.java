package Week2.OOPBasics;

public class CountryInfo {
    public String name;
    public String capital;
    public String primeMinister;
    public String[] language;

    public CountryInfo(String name, String capital, String primeMinister, String[] language){
        this.name = name;
        this.capital = capital;
        this.primeMinister = primeMinister;
        this.language = language;
    }

    @Override
    public String toString(){
        String info = this.name + "\n";
        for (String language : this.language){
            info = info + "\t" + language + "\n";
        }
        return info;
    }
}
