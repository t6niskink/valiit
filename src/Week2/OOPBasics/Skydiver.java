package Week2.OOPBasics;

public class Skydiver extends Athlete {

    @Override
    public void perform() {
        System.out.println("Falling from the sky...");
    }
}
