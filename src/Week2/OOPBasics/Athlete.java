package Week2.OOPBasics;

public abstract class Athlete {
    public String firstName;
    public String lastName;
    public int age;
    public String gender;
    public int height;
    public double weight;

    public abstract void perform();
}
