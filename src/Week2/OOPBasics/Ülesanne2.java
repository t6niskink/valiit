package Week2.OOPBasics;

import java.util.ArrayList;
import java.util.List;

public class Ülesanne2 {
    public static void main(String[] args) {
        // String on objekt
        // Stringide massiiv on ka objekt
        // String[] arr1 = {"aa", "nn"};
        // Object[] arr2 = {"aa", "nn"};
        // Object[] str1 = "Tere";

        List<CountryInfo> countries = new ArrayList<>();
        CountryInfo estonia = new CountryInfo(
                "Estonia",
                "Tallinn",
                "Jüri Ratas",
                new String[]{"Estonian", "Russian", "Finnish", "Latvian"}
        );
        countries.add(estonia);

        countries.add(new CountryInfo(
                "Latvia",
                "Riia",
                "Arturs Krišjānis Kariņš",
                new String[]{"Russian", "Latvian", "Livonian", "Latgalian"})
        );
        countries.add(new CountryInfo(
                "Sweden",
                "Stockholm",
                "Stefan Löfven",
                new String[]{"Finnish", "Swedish", "Southern Sami", "Meänkieli"})
        );

        for (CountryInfo country : countries){
            System.out.println(country);
        }
//        for (CountryInfo country : countries) {
//            System.out.println(country.name + ": ");
//            for (String language : country.language) {
//                System.out.println("       " + language);
//            }
//        }
    }
}
