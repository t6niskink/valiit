package Week2;

import java.io.CharArrayReader;
import java.util.ArrayList;
import java.util.List;

public class Day07 {
    public static void main(String[] args) {
        Human h1 = new Human("Ragnar");
        changeName(h1);             // Objektide puhul antakse edasi objekti referents, objektist koopiat ei tehta.
        System.out.println(h1.name);

        int mynumber = 222;
        changePrimitiveNumber(mynumber); // myNumber muutuja väärtusest tehakse koopia ja edasi antakse koopia.
        System.out.println(mynumber);

        List<Car> cars = new ArrayList<>();
        Car myFerrari = new FerrariF355();
        myFerrari.addDriver(h1);
        cars.add(myFerrari);

        cars.get(0).drive();


    }
    private static void changeName (Human x){
        x.name = "Kris";
    }

    private static void changePrimitiveNumber (int inputNumber){
        inputNumber = 555;
    }

}
