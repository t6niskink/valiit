package Week2.Looduspark;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;

public class LooduspargiApp {
    public static void main(String[] args) throws IOException {
        Looduspark.visits(args[0]);

        sortLowToHigh();
        System.out.println(maxVisits());

    }

    public static String maxVisits() {

        int maxVisitCount = 0;
        String maxVisitsPerDay = "";

        for (int i = 0; i < Looduspark.visits.size(); i++) {
            if (Integer.parseInt(Looduspark.visits.get(i).getVisits()) > maxVisitCount) {
                maxVisitCount = Integer.parseInt(String.valueOf(Looduspark.visits.get(i).getVisits()));
                maxVisitsPerDay = String.valueOf(Looduspark.visits.get(i).getDate());
            }
        }
        return maxVisitsPerDay;
    }


    public static void sortLowToHigh() {

        Comparator<Visit> instructionToSort = new Comparator<Visit>() {
            @Override
            public int compare(Visit o1, Visit o2) {
                // Kui o1 on suurem kui 02, siis negatiivne
                // Kui o1 on võrdne o1, siis 0
                // Kui o1 väiksem kui o2, siis positiivne
                if (Integer.parseInt(o2.getVisits()) > Integer.parseInt(o1.getVisits())) {
                    return -1;
                } else if (o2.getVisits() == o1.getVisits()) {
                    return 0;
                } else {
                    return 1;
                }
            }
        };
        Collections.sort(Looduspark.visits, instructionToSort);
        for (int i = 0; i < Looduspark.visits.size(); i++) {
            System.out.println(Looduspark.visits.get(i).getDate() + " " + Looduspark.visits.get(i).getVisits());
        }
    }
}
