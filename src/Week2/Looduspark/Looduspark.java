package Week2.Looduspark;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Looduspark {

    public static List<Visit> visits = new ArrayList<>();

    public static void main(String[] args) {

    }

    public static void visits(String filePath) throws IOException {
        List<String> visitRows = Files.readAllLines(Paths.get(filePath));
        for (String visitStr : visitRows) {
            String[] lineParts = visitStr.split(", ");

            String date = lineParts[0];
            String parkVisits = lineParts[1];

            Visit visits1 = new Visit(date, parkVisits);
            visits.add(visits1);
        }
    }
}
