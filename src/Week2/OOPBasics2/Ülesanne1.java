package Week2.OOPBasics2;

public class Ülesanne1 {

    public static void main(String[] args) {

       // Ülesanne 1:
        Dog estonianDog = new EstonianDog();
        estonianDog.bark();

        Dog germanDog = new GermanDog();
        germanDog.bark();

        Dog italianDog = new ItalianDog();
        italianDog.bark();

    }
}