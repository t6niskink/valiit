package Week2;

public class Day10Enums {
// Enumi idee on teha valikvastustega sisu


    private Gender personGender;

    public Gender getPersonGender() {
        return personGender;
    }

    public static void main(String[] args) {

        Gender myGender = Gender.MALE;
        Gender myGender1 = Gender.FEMALE;
        Gender[] gender = Gender.values();

        if (myGender == myGender) {
            System.out.println("Samast soost.");
        } else {
            System.out.println("Ei ole samast soost.");
        }

        Gender myGender3 = Gender.valueOf("MALE");
    }
}
