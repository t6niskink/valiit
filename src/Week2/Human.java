package Week2;

import java.lang.reflect.Constructor;

public class Human {
    public String name;     // Objekti muutuja
    public int age;         // Objekti muutuja
    public double weight;   // Objekti muutuja

    public static int humanCount = 0;// Klassimuutuja

    public Human(String name, int age, double weight) {
        this.name = name;   // Objekti muutuja
        this.age = age;     // Objekti muutuja
        this.weight = weight; // Objekti muutuja
        // Objektimuutujaid võib olla palju - igal objektil oma koopia.

        Human.humanCount++;  // Klassimuutuja (ainult üks eksemplaar.
    }

    public Human(String name) {
        this.name = name;
        Human.humanCount++;
    }

    public Human() {
    }

    public boolean isYoung() {
        return this.age < 100;
    }

    public static boolean isThisHumanYoung(Human human) {
        return human.age < 100;
    }
}
