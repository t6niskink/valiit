package Week2.Cryptor;

import Week2.OOPBank.Account;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class CryptoApp {

    public static void main(String[] args) throws IOException {

        List<String> alphabet = Files.readAllLines(Paths.get(args[0]));

        Cryptor encryptor = new Encryptor();
        encryptor.initAlphapet(alphabet);
        String encryptedMessage = encryptor.convertText(args[1]);
        System.out.println("Krüpteeritud sõnum: " + encryptedMessage);

        Cryptor decryptor = new Decryptor();
        decryptor.initAlphapet(alphabet);
        String decryptedMessage = decryptor.convertText(encryptedMessage);
        System.out.println("Krüpteeritud sõnum: " + decryptedMessage);

    }
}
