package Week2.Cryptor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Encryptor extends Cryptor {

    @Override
    public void initAlphapet(List<String> alphabet) {
//        cryptoMap.keySet();
//        cryptoMap.get();
//        cryptoMap.put();
//        String.split();
//        for (){}

        for (String line : alphabet) {
            // "A, Ü" --> "A", "Ü"
            String[] lineParts = line.split(", ");
            this.cryptoMap.put(lineParts[0], lineParts[1]);
        }
    }
}
