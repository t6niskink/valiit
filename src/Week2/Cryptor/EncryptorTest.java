package Week2.Cryptor;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class EncryptorTest {

    @Test
    public void testinitAlphapet() {
        // String "J, K" --> Map J -> K
        // String "A, E" --> Map A -> E
        List<String> alphabet = Arrays.asList("J, K", "A, E");
        Encryptor encryptor = new Encryptor();
        encryptor.initAlphapet(alphabet);
//        encryptor.cryptoMap.get("J");
        Assertions.assertTrue(encryptor.cryptoMap.get("J").equals("K"));
        Assertions.assertTrue(encryptor.cryptoMap.get("A").equals("E"));
    }
}
