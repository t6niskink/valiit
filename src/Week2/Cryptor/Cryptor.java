package Week2.Cryptor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Cryptor {

    protected Map<String, String> cryptoMap = new HashMap<>();

    public abstract void initAlphapet(List<String> alphabet);

    public String convertText(String inputText) {
        // Sisendtekst tuleb krüpteerida
        // .toCharArray()
        // String.valueOf()
        // cryptoMap.get()
        String convertedText = "";

        char[] textChars = inputText.toCharArray();
        for (char c : textChars) {
            String charString = String.valueOf(c);
            convertedText = convertedText + this.cryptoMap.get(charString);

        }
        return convertedText;
    }


}
