package Week2.Encapsulation;

public class App {

    public static void main(String[] args) {
        Human human1 = new Human("Tõnis", 23, 79);
        System.out.println("Minu nimi on: "+ human1.getName());
        human1 = new Human("Mart", human1.getAge(), human1.getWeight());
        System.out.println("Minu nimi on: "+ human1.getName());
    }
}
