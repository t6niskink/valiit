package Week2;

public class Day01 {

    public static void main(String[] args) {

        // Meetodi väljakutsumine
        int birthYear1 = retrieveBirthYear("39511206047");

        System.out.println(birthYear1);
        System.out.println(retrieveBirthYear("24507..."));

        System.out.println("Korrektne isikukood: " + isPersonalCodeCorrect("39511206047"));
        System.out.println("Korrektne isikukood: " + isPersonalCodeCorrect("39511206040"));

    }

    private static int retrieveBirthYear(String personalCode) {
        // 39511206047

        int centuryKey = Integer.parseInt(personalCode.substring(0, 1));
        int centuryYear = Integer.parseInt(personalCode.substring(1, 3));
        int century = 0;
        switch (centuryKey) {
            case 1:
                break;
            case 2:
                century = 1800;
                break;
            case 3:
                century = 1900;
                break;
            case 4:
                century = 1900;
            case 5:
                break;
            case 6:
                century = 2000;
                break;
            case 7:
                break;
            case 8:
                century = 2100;
                break;
            default:
                century = 0;
        }
        return century + centuryYear;
    }

    private static boolean isPersonalCodeCorrect(String personalCode) {
        if (personalCode == null || personalCode.length() != 11) {
            return false;
        }
        // Muudame isikukoodi täisarvude massiiviks

        int[] personalCodeDigits = new int[10];

        char[] personalCodeChars = personalCode.toCharArray();

        for (int i = 0; i < 10; i++) {
            String digitsString = String.valueOf(personalCodeChars[i]);
            int digit = Integer.parseInt(digitsString);
            personalCodeDigits[i] = digit;
        }

        int[] weights1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 1};

        int sum = 0;
        for (int i = 0; i < 10; i++){
            sum = sum + weights1[i] * personalCodeDigits[i];
        }

        int checkNumber = 0;
        if (sum % 11 != 10){
            checkNumber = sum % 11;
        } else {
            int[] weights2 = {3, 4, 5, 6, 7, 8, 9, 1, 2, 3};
            sum = 0;
            for (int i = 0; i < 10; i++){
                sum = sum + weights2[i] * personalCodeDigits[i];
            }
            if (sum % 11 != 10){
                checkNumber = sum % 11;
            } else {
                checkNumber = 0;
            }
        }
        char personalCodeLastChar = personalCode.charAt(10);
        String personalCodeLastCharStr = String.valueOf(personalCodeLastChar);
        int personalCodeLastDigit = Integer.parseInt(personalCodeLastCharStr);

        boolean isCorrect = personalCodeLastDigit == checkNumber;
        return isCorrect;
    }


}

