package Week2.OOPBank;


import java.io.IOException;
import java.util.Scanner;

public class BankApp {

    public static Scanner inputReader = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        AccountService.loadAccounts(args[0]); // See on text!

        while (true) {
            System.out.println();
            System.out.println("Sisesta käsklus:");
            String command = inputReader.nextLine();
            // TRANSFER
            String[] commandParts = command.split(" ");
            switch (commandParts[0].toUpperCase()) {

                case "BALANCE":
                    displayAccountDetails(commandParts);
                    break;

                case "TRANSFER":
                    makeTransfer(commandParts);
                    break;
                case "EXIT":
                    return;
                default:
                    System.out.println("Tundmatu käsklus.");
            }
        }
    }

    private static void displayAccountDetails(String[] commandParts) {
        if (commandParts.length == 2) {
            // Küsimine kontonumbri järgi
            // BALANCE 758818991
            Account account = AccountService.searchAccount(commandParts[1]);
            displayAccountDetails(account);

        } else if (commandParts.length == 3) {
            // Küsimine eesnime ja perenime järgi.
            // BALANCE Jean Waters
            Account account = AccountService.searchAccount(commandParts[1], commandParts[2]);
            displayAccountDetails(account);

        } else {
            System.out.println("Ebakorrektne käsklus!");
        }
    }

    private static void displayAccountDetails(Account accountToDisplay) {
        if (accountToDisplay != null) {
            System.out.println("----- Konto -----");
            System.out.println("Name: " + accountToDisplay.getFirstName() + " " + accountToDisplay.getLastName());
            System.out.println("Account number: " + accountToDisplay.getAccountNumber());
            System.out.println("Balance: " + accountToDisplay.getBalance());
        } else {
            System.out.println("Kontot ei leitud");
        }
    }

    private static void makeTransfer(String[] commandParts) {
        if (commandParts.length == 4) {
            ActionResponse response = AccountService.transfer(commandParts[1], commandParts[2], Double.parseDouble(commandParts[3]));
            System.out.println(response.getMessage());
            if (response.isActionSuccessful()) {
                System.out.println();
                System.out.println("Maksja konto andmed: ");
                displayAccountDetails(response.getFromAcount());
                System.out.println();
                System.out.println("Saaja konto andmed: ");
                displayAccountDetails(response.getToAccount());
            } else {
                System.out.println();
                System.out.println("****** VIGA! ******" + "\n" + response.getMessage());
                if (response.getFromAcount() != null)
                    System.out.println();
                System.out.println("Maksja konto detailandmed: ");
                displayAccountDetails(response.getFromAcount());
            }
        } else {
            System.out.println("Tundmatu käsklus.");
        }
    }
}
