package Week2.OOPBank;

public class ActionResponse {
    private boolean actionSuccessful;
    private Account fromAcount;
    private Account toAccount;
    private String message;

    public ActionResponse(boolean actionSuccessful, Account fromAcount, Account toAccount, String message) {
        this.actionSuccessful = actionSuccessful;
        this.fromAcount = fromAcount;
        this.toAccount = toAccount;
        this.message = message;
    }

    public boolean isActionSuccessful() {
        return actionSuccessful;
    }

    public Account getFromAcount() {
        return fromAcount;
    }

    public Account getToAccount() {
        return toAccount;
    }

    public String getMessage() {
        return message;
    }
}