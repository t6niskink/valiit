package Week2.OOPBank;
// Abimuutujad

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class AccountService {

    private static List<Account> accounts = new ArrayList<>();

    public static Account searchAccount(String firstName, String lastName) {
        for (int i = 0; i < accounts.size(); i++) {
            if (accounts.get(i).getFirstName().equalsIgnoreCase(firstName) && accounts.get(i).getLastName().equalsIgnoreCase(lastName)){
                return accounts.get(i);
            }
        }
        return null;
    }

    public static Account searchAccount(String accountNumber) {
        for (Account account : accounts) {
            if (account.getAccountNumber().equals(accountNumber)) {
                return account;
            }
        }
        return null;
    }

    public static ActionResponse transfer(String fromAccountNumber, String toAccountNumber, double sum){
        Account fromAccount = searchAccount(fromAccountNumber);
        Account toAccount = searchAccount(toAccountNumber);

        // Kui kasvõi üks kahest kontost on puudu, ei saa tehingut teostada.
        if (fromAccount == null || toAccount == null){
            return new ActionResponse(false, null, null, "Kontot/kontosid ei leitud!");
        }

        // Me ei saa teha ülekannet, kui kontol pole piisavalt vahendeid.
        if (fromAccount.getBalance() < sum){
            return new ActionResponse(false, fromAccount, toAccount, "Maksja kontol pole piisavalt vahendeid!");
        }

        // Maksja konto muudatused
        double fromAccountBalance = round(fromAccount.getBalance() - sum);
        fromAccount.setBalance(fromAccountBalance);

        // Saaja konto muudatused
        double toAccountBalance = round(toAccount.getBalance() + sum);
        toAccount.setBalance(toAccountBalance);

        return new ActionResponse(true, fromAccount, toAccount, "Transfer completed!");
    }

    public static void loadAccounts(String filePath) throws IOException {
        List<String> accountRows = Files.readAllLines(Paths.get(filePath));
        for (String acountStr : accountRows) {
            // String.split;
            // Double.parseDouble();

            String[] lineParts = acountStr.split(", ");

            String firstName = lineParts[0];
            String lastName = lineParts[1];
            String accountNumber = lineParts[2];
            double balance = Double.parseDouble(lineParts[3]);

            Account account = new Account(firstName, lastName, accountNumber, balance);
            accounts.add(account);
        }
    }

    private static double round (double value){
        return Math.round(value * 100.0) / 100.0;

    }
}
