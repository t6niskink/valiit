package Week4;

public class Exercise {
    public static final double VAT_RATE = 1.2;

    public static void main(String[] args) {

        System.out.println(deriveNetPrice(4.0));
        System.out.println(isPalindrome("Aias sadas saia"));
    }

    public static double deriveNetPrice(double grossPrice){
        if (grossPrice >= 0){
            double result = grossPrice / VAT_RATE;
            result = Math.round(result * 100.0) / 100.0;
            return result;
        }
        return 0;
    }

    public static boolean isPalindrome(String text){
        String reversedText = "";

        // Meetod 1 sõna ümberpööramiseks
//        for (int i = 0; i < text.length(); i++){
//            reversedText = text.charAt(i) + reversedText;
//        }

        // Meetod 2 sõna ümberpööramiseks
        StringBuilder builder = new StringBuilder(text);
        reversedText = builder.reverse().toString();


        return text.equalsIgnoreCase(reversedText);
    }

}
