package Vahetest;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

public class Ülesanne {

    public static void main(String[] args) {
        // Ülesanne 1
        double pi = 3.14159265358;
        pi = pi * 2;
        System.out.println(pi);

        // Ülesanne 2
        System.out.println(ristkülik());
        System.out.println(abc(3, 4));  // Mareku soovitud lahendus.

        // Ülesanne 4
        System.out.println(temp(31));
        System.out.println(temp(6));
        System.out.println(temp(-4));
        System.out.println(temp(11));
        System.out.println(temp(14));
        System.out.println(temp(17));
        System.out.println(temp(22));

        // Ülesanne 5
        Song laul1 = new Song(
                "Võta Aega",
                "Tanel Padar",
                "Live",
                2008,
                "Rock",
                "Iga suusataja\n" +
                        "Hoiab endas talve\n" +
                        "Oled sodur aina\n" +
                        "Pusima pead valvel\n" +
                        "Tere luuletaja\n" +
                        "Saada raamat trukki\n" +
                        "Spordimehed hommikuil\n" +
                        "Teevad kumme kukki\n" +
                        "Kuuled rock'n'rolli\n" +
                        "Nuud on oige aeg\n" +
                        "Tosta ules kaed ja kaelast rebi krae\n" +
                        "Vota aega veidi ringi vaadata\n" +
                        "Veel ei ole hilja jouab elada\n" +
                        "Ava silmad motle peaga kull sa naed\n" +
                        "Leiad roomu elus olemise vaest\n" +
                        "Seljal inglitiivad\n" +
                        "Kohuli sa magad siis\n" +
                        "Luurajatel tihti\n" +
                        "Voib olla saba taga\n" +
                        "Vahel tunned nagu\n" +
                        "Tobe orav ratta peal\n" +
                        "Koige jaoks ei jagu\n" +
                        "Koike tegema ei pea\n" +
                        "Kuuled rock'n'rolli.\n" +
                        "Kuigi alati sul koik ei lahe\n" +
                        "Koike ju ei saagi ette naha\n" +
                        "Ara heidutada sellest lase\n" +
                        "Rocki taiega\n" +
                        "Vota aega veidi ringi vaadata\n" +
                        "Veel ei ole hilja jouab elada\n" +
                        "Ava silmad motle peaga kull sa naed\n" +
                        "Leiad roomu elus olemise vaest");

        List<Song> laulud = Arrays.asList(laul1);
        System.out.println("------- Esimene laul: -------\n");
        System.out.println(laul1);


        // Ülesanne 3
        String[] sõna = {"maja", "Sadam", "Progemine"};
        for (String xxx: sõna) {
            char[] tähed = xxx.toCharArray();
            for (int i = 0; i < tähed.length; i++) {
                System.out.print("#");
            }
            System.out.println();
        }
    }

    // Ülesanne 2
    public static int ristkülik() {
        int a = 5;
        int b = 8;
        int s = a * b;
        return s;
    }
    public static int abc(int a, int b){ // Mareku soovitud lahendus. Sulgude sees on sisendparameetrid.
        return a * b;
    }


    // Ülesanne 4
    public static String temp(int args) {
        int temperatuur = Integer.parseInt(String.valueOf(args));
        if (temperatuur > 30) {
            System.out.println("Leitsak");
        } else if (25 < temperatuur && temperatuur < 30) {
            System.out.println("Rannailm");
        } else if (21 < temperatuur && temperatuur < 25) {
            System.out.println("Mõnus suveilm");
        } else if (15 < temperatuur && temperatuur < 21) {
            System.out.println("Jahe suveilm");
        } else if (10 < temperatuur && temperatuur < 15) {
            System.out.println("Jaani-ilm");
        } else if (0 < temperatuur && temperatuur < 10) {
            System.out.println("Telekavaatamise ilm");
        } else if (temperatuur < 0) {
            System.out.println("Suusailm");
        }
        return "";
    }
}
