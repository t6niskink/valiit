package Vahetest;

public class Song {
    String title;
    String artist;
    String album;
    int released;
    String genres;
    String lyrics;

    @Override
    public String toString() {
        return String.format("Laulu nimi: %s \n" +
                "\n Laulu autor: %s \n" +
                "\n Album: %s \n" +
                "\n Väljaantud: %d \n" +
                "\n Laulu stiil: %s \n" +
                "\n Laulu sõnad:\n%s", this.getTitle(),
                this.getArtist(), this.getAlbum(),
                this.getReleased(), this.getGenres(),
                this.getLyrics());
    }


    public Song(String title, String artist, String album, int released, String genres, String lyrics) {
        this.title = title;
        this.artist = artist;
        this.album = album;
        this.released = released;
        this.genres = genres;
        this.lyrics = lyrics;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public int getReleased() {
        return released;
    }

    public void setReleased(int released) {
        this.released = released;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }

    public String getLyrics() {
        return lyrics;
    }

    public void setLyrics(String lyrics) {
        this.lyrics = lyrics;
    }
}

